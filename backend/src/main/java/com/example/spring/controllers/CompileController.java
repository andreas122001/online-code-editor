package com.example.spring.controllers;

import com.example.spring.model.CompileRequest;
import com.example.spring.model.CompileResponse;
import com.example.spring.utils.ProcessTest;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(value = "/compile", method = RequestMethod.POST)
@EnableAutoConfiguration
@CrossOrigin
public class CompileController {

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public CompileResponse compile(final @RequestBody CompileRequest compileRequest) {
        String output;
        try {
            output = ProcessTest.compileAndRunJava(compileRequest.getCode());
        } catch (IOException e) {
            return new CompileResponse("(T)ERROR");
        }
        return new CompileResponse(output);
    }
}
