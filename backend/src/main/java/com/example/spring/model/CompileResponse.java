package com.example.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompileResponse {

    private final String output;

    public CompileResponse(@JsonProperty("output") String output) {
        this.output = output;
    }

    @JsonProperty("output")
    public String getOutput() {
        return output;
    }
}
