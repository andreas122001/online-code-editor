package com.example.spring.utils;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Objects;

public class ProcessTest {

    public static String compileAndRunJava(String code) throws IOException {
        code = StringEscapeUtils.escapeJava(code);

        ProcessBuilder pb = new ProcessBuilder("cmd").redirectErrorStream(true);
        Process proc = pb.start();

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        PrintWriter writer = new PrintWriter(proc.getOutputStream(), true);

        writer.println("echo off");
        writer.println("docker run --rm java-image /bin/bash -c \"mkdir code; cd code; echo -e '"+code+"' > file.java;javac *;java *\"");
//        writer.println("docker run --rm java-image /bin/bash -c \"mkdir code; cd code; echo -e '"+code+"' > file.java; cat file.java\"");
        writer.close();

        for (int i = 0; i<5; i++) stdInput.readLine();
        StringBuilder sb = new StringBuilder();
        String input;
        while ((input = stdInput.readLine())!=null) {
            sb.append(input).append("\n");
            System.out.println(input);
        }
        stdInput.close();

        return sb.toString();
    }
    
}
