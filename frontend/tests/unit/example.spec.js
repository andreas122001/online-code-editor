test('true is true', ()=> {
  expect(true).toBe(true)
})

test('false is false', ()=> {
  expect(false).toBe(false)
})
